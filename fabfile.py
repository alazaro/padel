# -*- coding: utf-8 -*-

from datetime import datetime

from fabric.api import *  # NOQA
from fabric.state import env


APP_PATH = '/www/padel/padel'


def prod():
    env.user = 'alazaro'
    env.hosts = ['lab.alazaro.es']


def deploy():
    local('git push')
    with cd(APP_PATH), prefix('. ../env/bin/activate'):
        run('git pull')
        run('pip install -r requirements.txt')
        foreman('collectstatic --noinput')
        foreman('migrate tournament')
    sudo('supervisorctl restart padel:*')


def backup():
    with cd(APP_PATH), prefix('. ../env/bin/activate'):
        backup_file = 'dump_%s.json.gz' % datetime.now().strftime('%Y%m%d%H%M')
        foreman('dumpdata --exclude=contenttypes | gzip > %s' % backup_file)
        get(backup_file, './backups/%(host)s/%(path)s')
        run('rm %s' % backup_file)


def backup_db():
    with cd(APP_PATH), prefix('. ../env/bin/activate'):
        backup_file = 'dump_%s.sql.gz' % datetime.now().strftime('%Y%m%d%H%M')
        run('pg_dump --no-password  --format custom --blobs --verbose --file'
            ' "%s" "padel"' % backup_file)
        get(backup_file, './backups/%(host)s/%(path)s')
        run('rm %s' % backup_file)


def foreman(command, **kwargs):
    run('foreman run ./manage.py %s' % command, **kwargs)
