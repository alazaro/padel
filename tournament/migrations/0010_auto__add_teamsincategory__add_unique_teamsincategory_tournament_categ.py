# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TeamsInCategory'
        db.create_table('tournament_teamsincategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tournament', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Tournament'], related_name='category_teams')),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Category'], related_name='tournament_teams')),
        ))
        db.send_create_signal('tournament', ['TeamsInCategory'])

        # Adding M2M table for field teams on 'TeamsInCategory'
        m2m_table_name = db.shorten_name('tournament_teamsincategory_teams')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('teamsincategory', models.ForeignKey(orm['tournament.teamsincategory'], null=False)),
            ('team', models.ForeignKey(orm['tournament.team'], null=False))
        ))
        db.create_unique(m2m_table_name, ['teamsincategory_id', 'team_id'])

        # Adding unique constraint on 'TeamsInCategory', fields ['tournament', 'category']
        db.create_unique('tournament_teamsincategory', ['tournament_id', 'category_id'])


        # Changing field 'Tournament.image'
        db.alter_column('tournament_tournament', 'image', self.gf('django.db.models.fields.files.ImageField')(null=True, max_length=100))

    def backwards(self, orm):
        # Removing unique constraint on 'TeamsInCategory', fields ['tournament', 'category']
        db.delete_unique('tournament_teamsincategory', ['tournament_id', 'category_id'])

        # Deleting model 'TeamsInCategory'
        db.delete_table('tournament_teamsincategory')

        # Removing M2M table for field teams on 'TeamsInCategory'
        db.delete_table(db.shorten_name('tournament_teamsincategory_teams'))


        # Changing field 'Tournament.image'
        db.alter_column('tournament_tournament', 'image', self.gf('django.db.models.fields.files.ImageField')(default='', max_length=100))

    models = {
        'tournament.category': {
            'Meta': {'object_name': 'Category'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'tournament.match': {
            'Meta': {'object_name': 'Match'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Category']", 'related_name': "'matches'", 'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'match_time': ('django.db.models.fields.DateTimeField', [], {}),
            'round_n': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Round']"}),
            'set1': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set2': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set3': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set4': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set5': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'team1': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Team']", 'related_name': "'match1'"}),
            'team2': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Team']", 'related_name': "'match2'"}),
            'winner': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['tournament.Team']", 'blank': 'True', 'related_name': "'matches_won'"}),
            'wo': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'tournament.player': {
            'Meta': {'object_name': 'Player'},
            'country': ('django_countries.fields.CountryField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'null': 'True', 'blank': 'True', 'max_length': '75'}),
            'fav_shot': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'license': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '40'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'points': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '10'}),
            'ranking': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '10'}),
            'short_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '15'})
        },
        'tournament.round': {
            'Meta': {'unique_together': "(('number', 'tournament', 'category'),)", 'object_name': 'Round'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['tournament.Category']"}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.SmallIntegerField', [], {}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Tournament']", 'related_name': "'rounds'"})
        },
        'tournament.team': {
            'Meta': {'unique_together': "(('player1', 'player2', 'active'),)", 'object_name': 'Team'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['tournament.Category']", 'related_name': "'teams'"}),
            'date_from': ('django.db.models.fields.DateField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '50'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'player1': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Player']", 'related_name': "'team1'"}),
            'player2': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Player']", 'related_name': "'team2'"})
        },
        'tournament.teamsincategory': {
            'Meta': {'unique_together': "(('tournament', 'category'),)", 'object_name': 'TeamsInCategory'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Category']", 'related_name': "'tournament_teams'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'teams': ('django.db.models.fields.related.ManyToManyField', [], {'null': 'True', 'to': "orm['tournament.Team']", 'related_name': "'tournament_categories'", 'symmetrical': 'False'}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Tournament']", 'related_name': "'category_teams'"})
        },
        'tournament.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['tournament.Category']", 'symmetrical': 'False'}),
            'ending_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'starting_date': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['tournament']