# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table('tournament_category', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('gender', self.gf('django.db.models.fields.CharField')(default='M', max_length=1)),
        ))
        db.send_create_signal('tournament', ['Category'])

        # Adding model 'Tournament'
        db.create_table('tournament_tournament', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('starting_date', self.gf('django.db.models.fields.DateField')()),
            ('ending_date', self.gf('django.db.models.fields.DateField')()),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('tournament', ['Tournament'])

        # Adding M2M table for field categories on 'Tournament'
        m2m_table_name = db.shorten_name('tournament_tournament_categories')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('tournament', models.ForeignKey(orm['tournament.tournament'], null=False)),
            ('category', models.ForeignKey(orm['tournament.category'], null=False))
        ))
        db.create_unique(m2m_table_name, ['tournament_id', 'category_id'])

        # Adding model 'Player'
        db.create_table('tournament_player', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('short_name', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True)),
            ('license', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('gender', self.gf('django.db.models.fields.CharField')(default='M', max_length=1)),
            ('points', self.gf('django.db.models.fields.IntegerField')(max_length=10, null=True, blank=True)),
            ('ranking', self.gf('django.db.models.fields.IntegerField')(max_length=10, null=True, blank=True)),
            ('date_of_birth', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('fav_shot', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('tournament', ['Player'])

        # Adding model 'Location'
        db.create_table('tournament_location', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('address', self.gf('django.db.models.fields.TextField')()),
            ('website', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('tournament', ['Location'])

        # Adding model 'Round'
        db.create_table('tournament_round', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('tournament', self.gf('django.db.models.fields.related.ForeignKey')(related_name='rounds', to=orm['tournament.Tournament'])),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Category'], null=True)),
            ('start_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('end_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal('tournament', ['Round'])

        # Adding unique constraint on 'Round', fields ['number', 'tournament', 'category']
        db.create_unique('tournament_round', ['number', 'tournament_id', 'category_id'])

        # Adding model 'Team'
        db.create_table('tournament_team', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('player1', self.gf('django.db.models.fields.related.ForeignKey')(related_name='team1', to=orm['tournament.Player'])),
            ('player2', self.gf('django.db.models.fields.related.ForeignKey')(related_name='team2', to=orm['tournament.Player'])),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('date_from', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('date_to', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name='teams', null=True, to=orm['tournament.Category'])),
        ))
        db.send_create_signal('tournament', ['Team'])

        # Adding unique constraint on 'Team', fields ['player1', 'player2', 'active']
        db.create_unique('tournament_team', ['player1_id', 'player2_id', 'active'])

        # Adding model 'Match'
        db.create_table('tournament_match', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('team1', self.gf('django.db.models.fields.related.ForeignKey')(related_name='match1', to=orm['tournament.Team'])),
            ('team2', self.gf('django.db.models.fields.related.ForeignKey')(related_name='match2', to=orm['tournament.Team'])),
            ('match_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(default=0, related_name='matches', to=orm['tournament.Category'])),
            ('round_n', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Round'])),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Location'])),
            ('set1', self.gf('django.db.models.fields.IntegerField')(max_length=2, null=True, blank=True)),
            ('set2', self.gf('django.db.models.fields.IntegerField')(max_length=2, null=True, blank=True)),
            ('set3', self.gf('django.db.models.fields.IntegerField')(max_length=2, null=True, blank=True)),
            ('set4', self.gf('django.db.models.fields.IntegerField')(max_length=2, null=True, blank=True)),
            ('set5', self.gf('django.db.models.fields.IntegerField')(max_length=2, null=True, blank=True)),
            ('wo', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('winner', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='matches_won', null=True, to=orm['tournament.Team'])),
        ))
        db.send_create_signal('tournament', ['Match'])


    def backwards(self, orm):
        # Removing unique constraint on 'Team', fields ['player1', 'player2', 'active']
        db.delete_unique('tournament_team', ['player1_id', 'player2_id', 'active'])

        # Removing unique constraint on 'Round', fields ['number', 'tournament', 'category']
        db.delete_unique('tournament_round', ['number', 'tournament_id', 'category_id'])

        # Deleting model 'Category'
        db.delete_table('tournament_category')

        # Deleting model 'Tournament'
        db.delete_table('tournament_tournament')

        # Removing M2M table for field categories on 'Tournament'
        db.delete_table(db.shorten_name('tournament_tournament_categories'))

        # Deleting model 'Player'
        db.delete_table('tournament_player')

        # Deleting model 'Location'
        db.delete_table('tournament_location')

        # Deleting model 'Round'
        db.delete_table('tournament_round')

        # Deleting model 'Team'
        db.delete_table('tournament_team')

        # Deleting model 'Match'
        db.delete_table('tournament_match')


    models = {
        'tournament.category': {
            'Meta': {'object_name': 'Category'},
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'tournament.location': {
            'Meta': {'object_name': 'Location'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'tournament.match': {
            'Meta': {'object_name': 'Match'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'related_name': "'matches'", 'to': "orm['tournament.Category']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Location']"}),
            'match_time': ('django.db.models.fields.DateTimeField', [], {}),
            'round_n': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Round']"}),
            'set1': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'set2': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'set3': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'set4': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'set5': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'team1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'match1'", 'to': "orm['tournament.Team']"}),
            'team2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'match2'", 'to': "orm['tournament.Team']"}),
            'winner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'matches_won'", 'null': 'True', 'to': "orm['tournament.Team']"}),
            'wo': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'tournament.player': {
            'Meta': {'object_name': 'Player'},
            'date_of_birth': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'fav_shot': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'license': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'points': ('django.db.models.fields.IntegerField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'ranking': ('django.db.models.fields.IntegerField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'})
        },
        'tournament.round': {
            'Meta': {'unique_together': "(('number', 'tournament', 'category'),)", 'object_name': 'Round'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Category']", 'null': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.SmallIntegerField', [], {}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rounds'", 'to': "orm['tournament.Tournament']"})
        },
        'tournament.team': {
            'Meta': {'unique_together': "(('player1', 'player2', 'active'),)", 'object_name': 'Team'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'teams'", 'null': 'True', 'to': "orm['tournament.Category']"}),
            'date_from': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'player1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'team1'", 'to': "orm['tournament.Player']"}),
            'player2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'team2'", 'to': "orm['tournament.Player']"})
        },
        'tournament.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['tournament.Category']", 'symmetrical': 'False'}),
            'ending_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'starting_date': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['tournament']