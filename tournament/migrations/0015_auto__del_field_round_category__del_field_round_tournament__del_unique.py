# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Round', fields ['number', 'tournament', 'category']
        db.delete_unique('tournament_round', ['number', 'tournament_id', 'category_id'])

        # Deleting field 'Round.category'
        db.delete_column('tournament_round', 'category_id')

        # Deleting field 'Round.tournament'
        db.delete_column('tournament_round', 'tournament_id')

        # Adding unique constraint on 'Round', fields ['number', 'category_tournament']
        db.create_unique('tournament_round', ['number', 'category_tournament_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Round', fields ['number', 'category_tournament']
        db.delete_unique('tournament_round', ['number', 'category_tournament_id'])

        # Adding field 'Round.category'
        db.add_column('tournament_round', 'category',
                      self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['tournament.Category']),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Round.tournament'
        raise RuntimeError("Cannot reverse this migration. 'Round.tournament' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Round.tournament'
        db.add_column('tournament_round', 'tournament',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='rounds', to=orm['tournament.Tournament']),
                      keep_default=False)

        # Adding unique constraint on 'Round', fields ['number', 'tournament', 'category']
        db.create_unique('tournament_round', ['number', 'tournament_id', 'category_id'])


    models = {
        'tournament.category': {
            'Meta': {'object_name': 'Category'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'tournament.match': {
            'Meta': {'object_name': 'Match'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'match_time': ('django.db.models.fields.DateTimeField', [], {}),
            'round_n': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Round']"}),
            'set1': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set2': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set3': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set4': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set5': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'team1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'match1'", 'to': "orm['tournament.Team']"}),
            'team2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'match2'", 'to': "orm['tournament.Team']"}),
            'winner': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'related_name': "'matches_won'", 'blank': 'True', 'to': "orm['tournament.Team']"}),
            'wo': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'tournament.player': {
            'Meta': {'object_name': 'Player'},
            'country': ('django_countries.fields.CountryField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'null': 'True', 'blank': 'True', 'max_length': '75'}),
            'fav_shot': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'license': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '40'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'points': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '10'}),
            'ranking': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '10'}),
            'short_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '15'})
        },
        'tournament.round': {
            'Meta': {'object_name': 'Round', 'unique_together': "(('number', 'category_tournament'),)"},
            'category_tournament': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rounds'", 'to': "orm['tournament.TeamsInCategory']"}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.SmallIntegerField', [], {}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        'tournament.team': {
            'Meta': {'object_name': 'Team', 'unique_together': "(('player1', 'player2', 'active'),)"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '50'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'player1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'team1'", 'to': "orm['tournament.Player']"}),
            'player2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'team2'", 'to': "orm['tournament.Player']"})
        },
        'tournament.teamsincategory': {
            'Meta': {'object_name': 'TeamsInCategory', 'unique_together': "(('tournament', 'category'),)"},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tournament_teams'", 'to': "orm['tournament.Category']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'teams': ('django.db.models.fields.related.ManyToManyField', [], {'null': 'True', 'symmetrical': 'False', 'related_name': "'tournament_categories'", 'to': "orm['tournament.Team']"}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'category_teams'", 'to': "orm['tournament.Tournament']"})
        },
        'tournament.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ending_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'starting_date': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['tournament']