# -*- coding: utf-8 -*-
from south.v2 import DataMigration


class Migration(DataMigration):

    def forwards(self, orm):
        players = orm.Player.objects.filter(
            photo__startswith='/static/').exclude(
                photo__startswith='/static/img')
        for player in players:
            player.photo = player.photo.url.replace('static/', 'static/img/')
            player.save()

    def backwards(self, orm):
        players = orm.Player.objects.filter(photo__startswith='/static/')

        for player in players:
            player.photo = player.photo.url.replace('static/img/', 'static/')
            player.save()

    models = {
        'tournament.category': {
            'Meta': {'object_name': 'Category'},
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'tournament.location': {
            'Meta': {'object_name': 'Location'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'website': ('django.db.models.fields.URLField', [], {'null': 'True', 'blank': 'True', 'max_length': '200'})
        },
        'tournament.match': {
            'Meta': {'object_name': 'Match'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Category']", 'default': '0', 'related_name': "'matches'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Location']"}),
            'match_time': ('django.db.models.fields.DateTimeField', [], {}),
            'round_n': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Round']"}),
            'set1': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set2': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set3': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set4': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set5': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'team1': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Team']", 'related_name': "'match1'"}),
            'team2': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Team']", 'related_name': "'match2'"}),
            'winner': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Team']", 'blank': 'True', 'related_name': "'matches_won'", 'null': 'True'}),
            'wo': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'tournament.player': {
            'Meta': {'object_name': 'Player'},
            'country': ('django_countries.fields.CountryField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'null': 'True', 'blank': 'True', 'max_length': '75'}),
            'fav_shot': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'license': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '40'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'points': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '10'}),
            'ranking': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '10'}),
            'short_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '15'})
        },
        'tournament.round': {
            'Meta': {'unique_together': "(('number', 'tournament', 'category'),)", 'object_name': 'Round'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Category']", 'null': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.SmallIntegerField', [], {}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Tournament']", 'related_name': "'rounds'"})
        },
        'tournament.team': {
            'Meta': {'unique_together': "(('player1', 'player2', 'active'),)", 'object_name': 'Team'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Category']", 'related_name': "'teams'", 'null': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '50'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'player1': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Player']", 'related_name': "'team1'"}),
            'player2': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Player']", 'related_name': "'team2'"})
        },
        'tournament.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['tournament.Category']", 'symmetrical': 'False'}),
            'ending_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'starting_date': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['tournament']
    symmetrical = True
