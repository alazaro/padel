# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Match.category'
        db.delete_column('tournament_match', 'category_id')

        # Adding field 'Round.category_tournament'
        db.add_column('tournament_round', 'category_tournament',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.TeamsInCategory'], related_name='rounds', default=orm.Tournament.objects.first().id),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Match.category'
        db.add_column('tournament_match', 'category',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='matches', to=orm['tournament.Category'], default=0),
                      keep_default=False)

        # Deleting field 'Round.category_tournament'
        db.delete_column('tournament_round', 'category_tournament_id')


    models = {
        'tournament.category': {
            'Meta': {'object_name': 'Category'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'default': "'M'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'tournament.match': {
            'Meta': {'object_name': 'Match'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'match_time': ('django.db.models.fields.DateTimeField', [], {}),
            'round_n': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Round']"}),
            'set1': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set2': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set3': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set4': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'set5': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'team1': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Team']", 'related_name': "'match1'"}),
            'team2': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Team']", 'related_name': "'match2'"}),
            'winner': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Team']", 'blank': 'True', 'related_name': "'matches_won'", 'null': 'True'}),
            'wo': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'tournament.player': {
            'Meta': {'object_name': 'Player'},
            'country': ('django_countries.fields.CountryField', [], {'null': 'True', 'blank': 'True', 'max_length': '2'}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'null': 'True', 'blank': 'True', 'max_length': '75'}),
            'fav_shot': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1', 'default': "'M'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'license': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '40'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'points': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '10'}),
            'ranking': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True', 'max_length': '10'}),
            'short_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '15'})
        },
        'tournament.round': {
            'Meta': {'unique_together': "(('number', 'tournament', 'category'),)", 'object_name': 'Round'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Category']", 'null': 'True'}),
            'category_tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.TeamsInCategory']", 'related_name': "'rounds'"}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.SmallIntegerField', [], {}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Tournament']", 'related_name': "'rounds'"})
        },
        'tournament.team': {
            'Meta': {'unique_together': "(('player1', 'player2', 'active'),)", 'object_name': 'Team'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '50'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'player1': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Player']", 'related_name': "'team1'"}),
            'player2': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Player']", 'related_name': "'team2'"})
        },
        'tournament.teamsincategory': {
            'Meta': {'unique_together': "(('tournament', 'category'),)", 'object_name': 'TeamsInCategory'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Category']", 'related_name': "'tournament_teams'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'teams': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['tournament.Team']", 'related_name': "'tournament_categories'", 'null': 'True'}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Tournament']", 'related_name': "'category_teams'"})
        },
        'tournament.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ending_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'starting_date': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['tournament']
