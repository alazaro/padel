# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing M2M table for field categories on 'Tournament'
        db.delete_table(db.shorten_name('tournament_tournament_categories'))

        # Deleting field 'Team.category'
        db.delete_column('tournament_team', 'category_id')


    def backwards(self, orm):
        # Adding M2M table for field categories on 'Tournament'
        m2m_table_name = db.shorten_name('tournament_tournament_categories')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('tournament', models.ForeignKey(orm['tournament.tournament'], null=False)),
            ('category', models.ForeignKey(orm['tournament.category'], null=False))
        ))
        db.create_unique(m2m_table_name, ['tournament_id', 'category_id'])

        # Adding field 'Team.category'
        db.add_column('tournament_team', 'category',
                      self.gf('django.db.models.fields.related.ForeignKey')(null=True, related_name='teams', to=orm['tournament.Category']),
                      keep_default=False)


    models = {
        'tournament.category': {
            'Meta': {'object_name': 'Category'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'tournament.match': {
            'Meta': {'object_name': 'Match'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'related_name': "'matches'", 'to': "orm['tournament.Category']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'match_time': ('django.db.models.fields.DateTimeField', [], {}),
            'round_n': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tournament.Round']"}),
            'set1': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'max_length': '2', 'blank': 'True'}),
            'set2': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'max_length': '2', 'blank': 'True'}),
            'set3': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'max_length': '2', 'blank': 'True'}),
            'set4': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'max_length': '2', 'blank': 'True'}),
            'set5': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'max_length': '2', 'blank': 'True'}),
            'team1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'match1'", 'to': "orm['tournament.Team']"}),
            'team2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'match2'", 'to': "orm['tournament.Team']"}),
            'winner': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'related_name': "'matches_won'", 'blank': 'True', 'to': "orm['tournament.Team']"}),
            'wo': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'tournament.player': {
            'Meta': {'object_name': 'Player'},
            'country': ('django_countries.fields.CountryField', [], {'null': 'True', 'max_length': '2', 'blank': 'True'}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'null': 'True', 'max_length': '75', 'blank': 'True'}),
            'fav_shot': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '100', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'license': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '20', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '40', 'blank': 'True'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'max_length': '100', 'blank': 'True'}),
            'points': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'max_length': '10', 'blank': 'True'}),
            'ranking': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'max_length': '10', 'blank': 'True'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'})
        },
        'tournament.round': {
            'Meta': {'object_name': 'Round', 'unique_together': "(('number', 'tournament', 'category'),)"},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['tournament.Category']"}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.SmallIntegerField', [], {}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rounds'", 'to': "orm['tournament.Tournament']"})
        },
        'tournament.team': {
            'Meta': {'object_name': 'Team', 'unique_together': "(('player1', 'player2', 'active'),)"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '50', 'blank': 'True'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'max_length': '100', 'blank': 'True'}),
            'player1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'team1'", 'to': "orm['tournament.Player']"}),
            'player2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'team2'", 'to': "orm['tournament.Player']"})
        },
        'tournament.teamsincategory': {
            'Meta': {'object_name': 'TeamsInCategory', 'unique_together': "(('tournament', 'category'),)"},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tournament_teams'", 'to': "orm['tournament.Category']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'teams': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'tournament_categories'", 'to': "orm['tournament.Team']", 'null': 'True'}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'category_teams'", 'to': "orm['tournament.Tournament']"})
        },
        'tournament.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ending_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'starting_date': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['tournament']