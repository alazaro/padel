# -*- coding: utf-8 -*-
from django.views.generic import TemplateView, DetailView

from . import models


class CategoryView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        context = super(CategoryView, self).get_context_data(*args, **kwargs)
        context['categories'] = models.TeamsInCategory.current.all()
        context['current_category'] = self._get_category()
        return context

    def _get_category(self):
        category = self.kwargs.get('category', -1)
        try:
            return models.TeamsInCategory.current.get(category=category)
        except models.TeamsInCategory.DoesNotExist:
            return models.TeamsInCategory.current.first()


class TeamDetail(DetailView):
    model = models.Team
    template_name = 'team.html'
    pk_url_kwarg = 'team'
    context_object_name = 'current_team'

    def get_context_data(self, **kwargs):
        context = super(TeamDetail, self).get_context_data(**kwargs)
        context['categories'] = models.TeamsInCategory.current.all()
        context['current_category'] = self._get_category()
        return context

    def _get_category(self):
        category = self.kwargs.get('category', -1)
        try:
            return models.TeamsInCategory.current.get(category=category)
        except models.TeamsInCategory.DoesNotExist:
            return models.TeamsInCategory.current.first()
