from django.contrib import admin
from . import models


def activate(modeladmin, request, queryset):
    queryset.update(active=True)


def deactivate(modeladmin, request, queryset):
    queryset.update(active=False)


class ActiveAdminMixin(object):
    list_display = ('active',)
    ordering = ('-active',)
    actions = [activate, deactivate]


class PlayerMixin(object):
    search_fields = ('player1__name', 'player2__name', 'name')
    list_display = ('__str__',) + ActiveAdminMixin.list_display


class TeamAdmin(ActiveAdminMixin, admin.ModelAdmin):
    search_fields = ('player1__name', 'player2__name', 'name')
    list_display = ('__str__',) + ActiveAdminMixin.list_display


class TournamentAdmin(ActiveAdminMixin, admin.ModelAdmin):
    search_fields = ('player1__name', 'player2__name', 'name')
    list_display = ('__str__',) + ActiveAdminMixin.list_display


class TeamsInCategoryAdmin(admin.ModelAdmin):
    filter_horizontal = ('teams', )


class MatchAdmin(admin.ModelAdmin):
    search_fields = (
        'team1__player1__name',
        'team2__player1__name',
        'team1__player2__name',
        'team2__player2__name',
        'team1__name',
        'team2__name',
    )


admin.site.register(models.Player)
admin.site.register(models.Team, TeamAdmin)
admin.site.register(models.Match, MatchAdmin)
admin.site.register(models.Tournament, TournamentAdmin)
admin.site.register(models.Round)
admin.site.register(models.Category)
admin.site.register(models.TeamsInCategory, TeamsInCategoryAdmin)
