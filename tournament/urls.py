# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from . import views


urlpatterns = patterns(
    '',
    url(r'stats/$', views.CategoryView.as_view(
        template_name='standings.html'), name='standings'),
    url(r'stats/(?P<category>\d*)/$', views.CategoryView.as_view(
        template_name='standings.html'), name='standings'),
    url(r'teams/$', views.CategoryView.as_view(
        template_name='teams.html'), name='teams'),
    url(r'teams/(?P<category>\d*)/$', views.CategoryView.as_view(
        template_name='teams.html'), name='teams'),
    url(r'teams/(?P<category>\d*)/(?P<team>\d*)/$', views.TeamDetail.as_view(),
        name='team_detail'),
)
