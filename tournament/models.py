# -*- coding: utf-8 -*-
import datetime
import itertools

from django.contrib.auth.models import AbstractBaseUser
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField


class CurrentTournamentManager(models.Manager):
    def get_queryset(self):
        queryset = super(CurrentTournamentManager, self).get_queryset()
        queryset = queryset.filter(tournament=Tournament.objects.get_current())
        return queryset


class TournamentManager(models.Manager):

    def get_current(self):
        result = Tournament.objects.filter(
            active=True).order_by('-starting_date')
        if result:
            return result[0]
        else:
            return None


class Category(models.Model):

    GENDER_CHOICES = (
        ('M', _("men's")),
        ('W', _("women's")),
        ('X', _("mixed"))
    )

    name = models.CharField(max_length=30)
    gender = models.CharField(choices=GENDER_CHOICES, default='M',
                              max_length=1)
    code = models.CharField(max_length=3)

    def __str__(self):
        return self.name

    @property
    def image(self):
        images = {
            'M': static('img/men.png'),
            'W': static('img/women.png'),
            'X': static('img/mixed.png')
        }
        return images[self.gender]


class Tournament(models.Model):
    name = models.CharField(max_length=20)
    starting_date = models.DateField()
    ending_date = models.DateField()
    image = models.ImageField(upload_to='images/tournament', blank=True,
                              null=True)
    active = models.BooleanField(default=True)

    objects = TournamentManager()

    def __str__(self):
        return self.name


class Player(AbstractBaseUser):
    GENDER_CHOICES = (
        ('M', _("male")),
        ('F', _("female")),
        ('O', _("other"))
    )

    name = models.CharField(max_length=50)
    short_name = models.CharField(max_length=15, blank=True)
    license = models.CharField(max_length=20, blank=True, null=True)
    photo = models.ImageField(upload_to='images/player', blank=True, null=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES,
                              default='M')
    points = models.IntegerField(max_length=10, null=True, blank=True)
    ranking = models.IntegerField(max_length=10, null=True, blank=True)
    date_of_birth = models.DateField(blank=True, null=True)
    fav_shot = models.CharField(max_length=100, blank=True, null=True)
    country = CountryField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=40, null=True, blank=True)

    def __str__(self):
        return self.__unicode__().encode('utf-8')

    def __unicode__(self):
        return self.short_name or self.name

    def save(self):
        if not self.photo:
            self.photo = static('img/default_%s.png' % self.gender)
        super(Player, self).save()

    @property
    def teams(self):
        return Team.objects.filter(
            models.Q(player1=self) | models.Q(player2=self) &
            models.Q(active=True))


class CurrentTournamentCategoryManager(models.Manager):
    def get_queryset(self):
        queryset = super(CurrentTournamentCategoryManager, self).get_queryset()
        queryset = queryset.filter(
            category_tournament__tournament=Tournament.objects.get_current())
        return queryset


class Round(models.Model):
    number = models.SmallIntegerField()
    category_tournament = models.ForeignKey(
        'TeamsInCategory', related_name='rounds')
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)

    objects = CurrentTournamentCategoryManager()

    def is_active_at(self, date):
        return self.start_date <= date <= self.end_date

    def is_active_now(self):
        return self.is_active_at(datetime.date.today())

    class Meta:
        unique_together = ('number', 'category_tournament')

    def __str__(self):
        return '%s round - %s' % (self.number, self.category_tournament)


class ActiveTeamsManager(models.Manager):
    def get_queryset(self):
        return super(ActiveTeamsManager, self).get_queryset(
            ).filter(active=True)


class Team(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    player1 = models.ForeignKey(Player, related_name='team1')
    player2 = models.ForeignKey(Player, related_name='team2')
    active = models.BooleanField(default=True)
    date_from = models.DateField(auto_now_add=True)
    date_to = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to='img/team', blank=True, null=True)

    objects = models.Manager()
    active_teams = ActiveTeamsManager()

    class Meta:
        unique_together = ('player1', 'player2', 'active')

    def __unicode__(self):
        return (self.name or u'%s - %s' % (unicode(self.player1)[:15],
                unicode(self.player2)[:15]))

    def __str__(self):
        return self.__unicode__().encode('utf-8')

    def _get_category_tournament(self, category, tournament):
        if not tournament:
            tournament = Tournament.objects.get_current()

        tournament_categories = TeamsInCategory.objects.filter(
            tournament=tournament)
        if category:
            tournament_category = tournament_categories.get(category=category)
        else:
            match = self.match1.first() or self.match2.first()
            if match:
                category = match.round_n.category_tournament.category
                tournament_category = tournament_categories.get(category=category)
            else:
                return None

        return tournament_category

    def matches(self, category=None, tournament=None):
        tournament_category = self._get_category_tournament(
            category, tournament)
        if tournament_category:
            return Match.objects.filter(
                models.Q(round_n__category_tournament=tournament_category) &
                (models.Q(team1=self) | models.Q(team2=self)))
        else:
            return []

    def played_matches(self, category=None, tournament=None):
        matches = self.matches(category, tournament).exclude(
            winner=None)
        return matches

    def won_matches(self, category=None, tournament=None):
        tournament_category = self._get_category_tournament(
            category, tournament)

        return self.matches_won.filter(
            round_n__category_tournament=tournament_category)

    def lost_matches(self, category=None, tournament=None):
        return self.matches(category, tournament).exclude(
            id__in=self.won_matches(category, tournament).values(
                'id')).exclude(winner=None).distinct()

    def diff(self, category=None, tournament=None):
        return self.won_sets(category, tournament) - self.lost_sets(
            category, tournament)

    def points(self, category=None, tournament=None):
        return self.won_matches(
            category, tournament).count() * 3 + self.lost_matches(
            category, tournament).count()

    def won_games(self, category=None, tournament=None):
        tournament_category = self._get_category_tournament(
            category, tournament)
        matches_team1 = self.match1.filter(
            round_n__category_tournament=tournament_category)
        matches_team2 = self.match2.filter(
            round_n__category_tournament=tournament_category)
        won_as_team1 = sum(
            itertools.chain.from_iterable((
                int(r[0]) for r in m.result_tuple)
                for m in matches_team1))
        won_as_team2 = sum(
            itertools.chain.from_iterable((
                int(r[1]) for r in m.result_tuple)
                for m in matches_team2))
        return won_as_team1 + won_as_team2

    def lost_games(self, category=None, tournament=None):
        tournament_category = self._get_category_tournament(
            category, tournament)
        matches_team1 = self.match1.filter(
            round_n__category_tournament=tournament_category)
        matches_team2 = self.match2.filter(
            round_n__category_tournament=tournament_category)
        lost_as_team1 = sum(
            itertools.chain.from_iterable((
                int(r[1]) for r in m.result_tuple)
                for m in matches_team1))
        lost_as_team2 = sum(
            itertools.chain.from_iterable((
                int(r[0]) for r in m.result_tuple)
                for m in matches_team2))
        return lost_as_team1 + lost_as_team2

    def won_sets(self, category=None, tournament=None):
        tournament_category = self._get_category_tournament(
            category, tournament)
        matches_team1 = self.match1.filter(
            round_n__category_tournament=tournament_category)
        matches_team2 = self.match2.filter(
            round_n__category_tournament=tournament_category)
        won_as_team1 = sum(
            itertools.chain.from_iterable((
                1 if r[0] > r[1] else 0
                for r in m.result_tuple) for m in matches_team1))
        won_as_team2 = sum(
            itertools.chain.from_iterable((
                1 if r[0] < r[1] else 0
                for r in m.result_tuple) for m in matches_team2))
        return won_as_team1 + won_as_team2

    def lost_sets(self, category=None, tournament=None):
        tournament_category = self._get_category_tournament(
            category, tournament)
        matches_team1 = self.match1.filter(
            round_n__category_tournament=tournament_category)
        matches_team2 = self.match2.filter(
            round_n__category_tournament=tournament_category)
        lost_as_team1 = sum(
            itertools.chain.from_iterable((
                1 if r[0] < r[1] else 0
                for r in m.result_tuple) for m in matches_team1))
        lost_as_team2 = sum(
            itertools.chain.from_iterable((
                1 if r[0] > r[1] else 0
                for r in m.result_tuple) for m in matches_team2))
        return lost_as_team1 + lost_as_team2

    def save(self, **kwargs):
        if not self.photo:
            self.photo = static('team_default.png')
        super(Team, self).save(**kwargs)


class TeamsInCategory(models.Model):
    tournament = models.ForeignKey(
        Tournament, default=Tournament.objects.get_current,
        related_name='category_teams', limit_choices_to={'active': True})
    category = models.ForeignKey(
        Category, related_name='tournament_teams')
    teams = models.ManyToManyField(
        Team, related_name='tournament_categories',
        limit_choices_to={'active': True}, null=True)

    current = CurrentTournamentManager()
    objects = models.Manager()

    @property
    def sorted_teams(self):
        return sorted(
            self.teams.all(), key=lambda t: t.points(), reverse=True)

    @property
    def active(self):
        return self.tournament.active

    class Meta:
        unique_together = ('tournament', 'category')

    def __str__(self):
        return '%s - %s' % (self.tournament, self.category)


class MatchManager(models.Manager):
    def get_queryset(self, *args, **kwargs):
        queryset = super(MatchManager, self).get_queryset(*args, **kwargs)
        return queryset.filter(
            round_n__category_tournament__tournament=
            Tournament.objects.get_current())


class Match(models.Model):
    team1 = models.ForeignKey(Team, related_name='match1')
    team2 = models.ForeignKey(Team, related_name='match2')
    match_time = models.DateTimeField()
    round_n = models.ForeignKey(Round)

    set1 = models.IntegerField(
        max_length=2, blank=True, null=True)
    set2 = models.IntegerField(
        max_length=2, blank=True, null=True)
    set3 = models.IntegerField(
        max_length=2, blank=True, null=True)
    set4 = models.IntegerField(
        max_length=2, blank=True, null=True)
    set5 = models.IntegerField(
        max_length=2, blank=True, null=True)

    wo = models.BooleanField(default=False)

    winner = models.ForeignKey(
        Team, related_name='matches_won', blank=True, null=True)

    objects = MatchManager()

    def __str__(self):
        return '%s vs. %s' % (self.team1, self.team2)

    def result(self, team='default'):
        output = ''
        for i in range(1, 6):
            set_result = getattr(self, 'set%s' % i)

            if set_result:
                if team == 2:
                    set_result = reversed(str(set_result))
                else:
                    set_result = str(set_result)

                if output:
                    output += ' - '
                output += '%s:%s' % tuple(set_result)

        return output

    def result_2(self):
        return self.result(2)

    @property
    def result_tuple(self):
        result = []
        for i in range(1, 6):
            set_result = getattr(self, 'set%s' % i)
            if set_result:
                set_result = tuple(str(set_result))
                result.append(set_result)
        return result

    def update_winner(self):
        result = self.result_tuple
        sets_won_1 = 0
        sets_won_2 = 0
        for set_ in result:
            if 6 > int(set_[0]) and 6 > int(set_[1]):
                # Match not finished
                self.winner = None
                return 0

            if set_[0] > set_[1]:
                sets_won_1 += 1
            else:
                sets_won_2 += 1

        if sets_won_1 > sets_won_2:
            self.winner = self.team1
            return 1
        elif sets_won_2 > sets_won_1:
            self.winner = self.team2
            return 2
        else:
            self.winner = None
            return 0

    def save(self):
        self.update_winner()
        super(Match, self).save()
