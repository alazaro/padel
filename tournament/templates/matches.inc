{% for round in rounds %}
    <table class="table table-striped table-responsive">
        <thead>
            <tr>
                <th>{{ round }}</th>
                <th>Wo?</th>
                <th>Result</th>
            </tr>
        </thead>
        <tbody>
        {% for match in round.match_set.all %}
            <tr>
                <td>{{ match }}</td>
                <td>{{ match.wo|yesno:"X," }}</td>
                <td>{{ match.result }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% endfor %}
