<div class="table-teams col-md-4" style="background-image: url('{{ category.image }}')">
    <h2 class="highlighted">{{ category.category }}</h2>
    <table class="table">
        <tbody>
        {% for team in category.sorted_teams %}
            <tr>
                <td>{{ forloop.counter }}</td>
                <td><a href="{% url "team_detail" category.category.id team.id %}">
                    {{ team }}</a></td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
</div>
<div class="table-stats col-md-8">
    <table class="table">
        <thead>
            <tr>
                <th>POINTS</th>
                <th>Played Matches</th>
                <th>Won Matches</th>
                <th>Lost Matches</th>
                <th>Won Sets</th>
                <th>Lost Sets</th>
            </tr>
        </thead>
        <tbody>
        {% for team in category.sorted_teams %}
            <tr>
                <td>{{ team.points | default:"0" }}</td>
                <td>{{ team.played_matches.count | default:"0" }}</td>
                <td>{{ team.won_matches.count | default:"0" }}</td>
                <td>{{ team.lost_matches.count | default:"0" }}</td>
                <td>{{ team.won_sets | default:"0" }}</td>
                <td>{{ team.lost_sets | default:"0" }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
</div>
