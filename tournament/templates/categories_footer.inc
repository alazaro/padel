    {% for category in categories %}
    <div class="col-md-3">
        <a href="{% block category_link %}{% url "standings" category.id %} {% endblock %}">
            <img class="img-responsive center-block category-img {% if category == current_category %} selected {% endif %}" src="{{ category.image }}" alt="{{ category.name }}">
            <p class="text-center icon-label">{{ category.name }}</p>
        </a>
    </div>
    {% endfor %}
