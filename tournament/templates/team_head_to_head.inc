{% load i18n %}
        <div class="row">
            <table class="head-to-head table">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Results</th>
                    </tr>
                </thead>
                <tbody>
                {% for match in team.matches %}
                <tr>
                <td class="{% if match.winner == team %}green{% elif match.winner %}red{% else %}dark{% endif %}">&nbsp;</td>
                    {% if match.team1 != team %}
                    <td>{{ match.team1.player1 }}<br>
                        {{ match.team1.player2 }}</td>
                    <td>{{ match.result_2 }}</td>
                    {% else %}
                    <td>{{ match.team2.player1 }}<br>
                        {{ match.team2.player2 }}</td>
                    <td>{{ match.result }}</td>
                    {% endif %}
                </tr>
                {% empty %}
                <tr>
                    <td colspan="3">No matches played yet</td>
                </tr>
                {% endfor %}
                </tbody>
            </table>
        </div>
