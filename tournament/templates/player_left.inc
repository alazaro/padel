{% load i18n %}
        <div class="row">
            <img class="player-image center-block" src="{{ player.photo.url }}">
        </div>
        <div class="row">
            <table class="player-data table">
                <thead>
                    <tr>
                        <th>{{ player.name }}</th>
                        <th class="flag">{% if player.country.flag %}
                        <img src="{{ player.country.flag }}">{% endif %}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td>{% trans "Favorite shot:" %} {{ player.fav_shot|default:'N/A' }}</td>
                        <td>{{ player.date_of_birth|default:'N/A' }}</td>
                    </tr>
                    <tr>
                        <td>{% trans "Current Ranking:" %} {{ player.ranking|default:'N/A' }}</td>
                        <td>{{ player.points|default:0 }} {% trans "points" %}</td>
                    </tr>
                    <tr>
                        <td>{% trans "Best Ranking: " %} {{ player.best_ranking|default:'N/A' }}</td>
                        <td>{{ player.best_ranking_date|date|default:'N/A' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

