{% load i18n %}
        <div class="row">
            <table class="team-stats table">
                <tbody>
                {% for team_stats in current_category.sorted_teams %}
                <tr {% if team_stats == team %}class="dark"{% endif %}>
                    <td>{{ forloop.counter }}</td>
                    <td>{{ team_stats.player1 }}<br>
                    {{ team_stats.player2 }}</td>
                </tr>
                {% empty %}
                <tr>
                    <td colspan="2">No matches played yet</td>
                </tr>
                {% endfor %}
                </tbody>
            </table>
        </div>
